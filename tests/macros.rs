use std::error::Error;
use std::time::Duration;

use lrau::{Permissions, User};
#[macro_use]
extern crate lrsvr;

#[test]
pub fn lrau_sql_select_test() -> Result<(), Box<dyn Error>> {
    let permissions: Permissions =
        serde_json::from_str(include_str!("macros.json")).unwrap();

    let mut user: User =
        User::new("username".to_string(), "1234", permissions)?;

    user.log_in("1234", Duration::MAX)?;

    assert_eq!(
        lrau_sql_select! {
            "table",
            &user,
            {
                &["auth"], false => "auth",
                &["no_auth"], false => "no_auth",
                &["mut"], false => "mut",
            }
        },
        "SELECT auth,mut FROM table",
    );

    assert_eq!(
        lrau_sql_select!(
            "table",
            &user,
            {
                &["auth"], true => "auth",
                &["no_auth"], true => "no_auth",
                &["mut"], true => "mut",
            }
        ),
        "SELECT mut FROM table",
    );

    Ok(())
}

#[test]
pub fn lrau_sql_update_test() -> Result<(), Box<dyn Error>> {
    let permissions: Permissions =
        serde_json::from_str(include_str!("macros.json")).unwrap();

    let mut user: User =
        User::new("username".to_string(), "1234", permissions)?;

    user.log_in("1234", Duration::MAX)?;

    let query_data = lrau_sql_update! {
        "table",
        &user,
        "WHERE id = 0",
        {
            &["auth"] => "auth",
            &["no_auth"] => "no_auth",
            &["mut"] => "mut",
        }
    };

    assert_eq!(query_data, "UPDATE table SET mut = $1 WHERE id = 0",);

    drop(lrau_sql_bind! {
        query_data,
        &user,
        {
            &["auth"], true => 1,
            &["no_auth"], true => 2,
            &["mut"], true => 3
        }
    });

    // Slight problem: I don't really know how to test this part.
    // By calling query.sql() you recieve the sql you put in, no the
    // escaped sql. This is problematic as it means we cannot test that
    // we have binded the parameters correctly.
    //
    // WILL LOOK INTO!!

    Ok(())
}

#[test]
pub fn lrau_sql_insert_test() -> Result<(), Box<dyn Error>> {
    let permissions: Permissions =
        serde_json::from_str(include_str!("macros.json")).unwrap();

    let mut user: User =
        User::new("username".to_string(), "1234", permissions)?;

    user.log_in("1234", Duration::MAX)?;

    let query_data = lrau_sql_insert! {
        "table",
        &user,
        {
            &["auth"] => "auth",
            &["no_auth"] => "no_auth",
            &["mut"] => "mut",
        }
    };

    assert_eq!(query_data, "INSERT INTO table (mut) VALUES ($1)",);

    drop(lrau_sql_bind! {
        query_data,
        &user,
        {
            &["auth"], true => 1,
            &["no_auth"], true => 2,
            &["mut"], true => 3
        }
    });

    // Slight problem: I don't really know how to test this part.
    // By calling query.sql() you recieve the sql you put in, no the
    // escaped sql. This is problematic as it means we cannot test that
    // we have binded the parameters correctly.
    //
    // WILL LOOK INTO!!

    Ok(())
}
