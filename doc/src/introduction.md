# Introduction

Libre Register is a register program for schools, specifically aimed at high schools (or whatever your country considers the school you are in from 11 to 16.) It is currently under very heavy devlopment, and is no where near ready to even be run in a production environment.

This book is in two parts:
 * Setting it up.
 * API calls and suchlike.

Feel free to contribute over on [Gitlab](https://gitlab.com/libre-register/libre-register-server/) ☺.
