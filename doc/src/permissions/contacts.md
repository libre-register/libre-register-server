# Contacts `/contacts`

`/contacts` is the best path for all contacts. The properties are closely mirroed to that of VCard.

 * `/contacts/UID` - Contact’s uid. Should be enabled - without it they don’t have permission to any contacts. Also is used to determine if the user can insert any contacts (not that this gives full permission to every property.)
 * `/contacts/N` - Contact’s name.
     * `/contacts/N/given` - Contact’s given name.
     * `/contacts/N/family` - Contact’s family name.
     * `/contacts/N/additional` - Contact’s middle names.
     * `/contacts/N/prefixes` - Contact’s name prefixes (ie ‘Mr’ or ‘Mrs’.)
     * `/contacts/N/suffixes` - Contact’s name suffixes (ie ‘Esq.’.)
 * `/contacts/NICKNAME` - Contact’s nickname.
 * `/contacts/ANNIVERSARY` - Contact’s anniversary.
 * `/contacts/BDAY` - Contact’s birthday.
 * `/contacts/PHOTO` - Contact’s photo.
 * `/contacts/TITLE` - Contact’s job title.
 * `/contacts/ROLE` - Contact’s job role.
 * `/contacts/ORG` - Contact’s organisational information.
     * `/contacts/ORG/org` - Contact’s organisation
     * `/contacts/ORG/unit` - Contact’s unit with their organisation (ie Maths department.)
     * `/contacts/ORG/office` - Contact’s office (ie blue-red Maths office.).
 * `/contacts/LOGO` - Contact’s orgnisation’s logo.
 * `/contact/CI` - Contact’s contact information.
     * `/contacts/CI/work` - Contact’s work contact information.
     * `/contacts/CI/home` - Contact’s personal contact information (recommended to be disabled.)
 * `/contacts/ADR` - Contact’s address.
     * `/contacts/ADR/work` - Contact’s work address.
     * `/contacts/ADR/home` - Contact’s personal address (recommended to be disabled.)
