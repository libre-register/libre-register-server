# Logins `/login`

`/login` is the base path for all login based permissions.

 * `/login/permissions` - Login permissions
 * `/login/username` - Login usernames
 * `/login/password` - No you cannot get a user’s password. But you *can* reset passwords with `mut` privalliges.
 * `/login/uid` - The uid links logins to other things such as contacts. Should be enabled to non `mut`.
