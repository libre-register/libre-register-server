# Permissions

In Libre Register, permissions are configured through [Lrau](https://crates.io/crates/lrau). This documentation will list all of the paths keys so that you can work with them.
