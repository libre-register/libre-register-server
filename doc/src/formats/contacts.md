# Contacts Format

Contacts are in the form of a [VCard](https://en.wikipedia.org/wiki/VCard), with the following properties only appearing once:

 * Uid (Obviously) `UID`
 * Name `N`
 * Nickname `NICKNAME`
 * Anniversary `ANNIVERSARY`
 * Birthday `BDAY`
 * Photo `PHOTO`
 * Title `TITLE`
 * Role `ROLE`
 * Organisation `ORG`
 * Logo `LOGO`
 * Work Address `ADR` with `type=work`
 * Home Address `ADR` with `type=home`

And the following appearing more than once:

 * Email `EMAIL`
 * Telephone `TEL`
 * Discord `X-DISCORD`
 * Matrix `X-MATIX`
 * Skype `X-SKYPE`
 * Aim `X-AIM`
 * Jabber `X-JABBER`
 * Icq `X-ICQ`
 * Groupwise `X-GROUPWISE`
 * GaduGadu `X-GADUGADU`
 * Other/Impp `IMPP`

*Adapted from the list found in [Evolution](https://wiki.gnome.org/Apps/Evolution).*

Some properties, such as `GENDER` where not included because of opinionated reasons. If you can put forth a convincing argument about why they are necessary open up an issue.
