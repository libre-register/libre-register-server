# Index `/`

 * Method: `GET`
 * Path: `/`
 * Response Format: `application/json`
 * Returns: A Json value (see below)

## Response

A json map in the form of:

```json
{
    "id": "LIBRE_REGISTER_SERVER"
    "version": "0.1.0"
}
```

The ID will (in a standard Libre Register server always be `LIBRE_REGISTER_SERVER`, although this can and should change is forks with additional/restricted features.

The version is any [semver](semver.org) version.

# Plaintext Index `/`

 * Method: `GET`
 * Path: `/`
 * Response Format: `text/plain`
 * Returns: A Json value (see below)

## Response

```
LIBRE_REGISTER_SERVER VERSION
```

Where version is a semver version.
