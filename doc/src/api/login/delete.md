# Delete Login `/contacts/<uid>?<session>`

 * Method: `DELETE`
 * Path: `/login/users/<username>?<session>`
     * Where `<username>` is the username of the user, who shall be deleted.
     * Where `<session>` is currently logged in user’s session.
 * Permissions (nearly all of them minus `/contacts/CI/{home,work}):
     * `/login/permissions`
     * `/login/username`
     * `/login/password`
 * Returns: Nothing (`204`)

## Response

 * `204` - Success.
 * `500` - Internal server error.
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `404` - Username not found.
