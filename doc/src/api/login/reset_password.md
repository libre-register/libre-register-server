# Reset Password `/login/users/<user>/password?<session>`

 * Method: `PATCH`
 * Request Format: the user’s new password.
 * Path: `/login/users/<user>/password?<session>`
     * Where `<user>` is the username of the user who’s password will be reset.
     * Where `<session` is the currently logged in user’s session
 * Permissions: `/login/password` with `mut` privaliges *unless* reseting your own password, which requires no permissions.
 * Returns: nothing (`204`)

## Request Format

The format is an unhashed password with a length greater than 0 and smaller that 2^32-1.

## Response

 * `204` - Success.
 * `400` - Password too long or too short.
 * `404` - Username not found.
 * `500` - Internal server error.
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
