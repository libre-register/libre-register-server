# Get Uid `/login/users/<username>/uid?<session>`

 * Method: `GET`
 * Path: `/login/users/<username>/uid?<session>`
 * Returns: The user’s uid
 * Permissions: `/login/uid`, unless getting your own uid.

### Response

#### Success

On success you will get code `200`, if the user has a uid, or `204` if the user does not. I fthe user has a uid it will be returned as follows:
```json
xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx
```

#### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` with the content `INVALID_SESSION` - Indicates the user could not get their permissions as their session is invalid.
 * `404` - Can’t find the permissions.
