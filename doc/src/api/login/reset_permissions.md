# Reset Permissions `/login/users/<user>/permissions?<session>`

 * Method: `PATCH`
 * Request Format: the user’s new password.
 * Path: `/login/users/<user>/permissions?<session>`
     * Where `<user>` is the username of the user who’s password will be reset.
     * Where `<session` is the currently logged in user’s session
 * Permissions: `/login/permissions` with `mut` privaliges.
 * Returns: nothing (`204`)

## Request Format

The request should have a json LrAU permission. It should be the entire permissions, not a subset.

## Response

 * `204` - Success.
 * `400` - Json cannot be parsed.
 * `500` - Internal server error.
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `404` - Username not found.
