# Log in `/login/login`

 * Method: `POST`
 * Format: `application/json`
     * With key `username` and `password`.
 * Path: `/login/login`
 * Returns: A single Uuid, which is the session. This session will have to be sent with subsequent requests to manage permissions.

## Format

The format is JSON and has the keys `username` and `password`:

```json
{
    "username": "bob",
    "password": "1234",
}
```

## Response

### Success

On success the response will be status code `200` and a uuid v4 in the form of:

```
xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx
```

[Wikipedia](https://en.wikipedia.org/wiki/Universally_unique_identifier)

### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` with the content `INVALID_SESSION` - Indicates the user could not be logged in because of incorrect credentials. Best to display an *“Invalid username or password”* message.
 * `400` - Malformed JSON
