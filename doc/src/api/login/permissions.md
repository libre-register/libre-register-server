# Permissions 

 * Method: `GET`
 * Path: `/login/users/by_session/<session>/permissions` or `/login/users/by_username/<username>/permissions?<session>`
 * Returns: A JSON map containing the user’s permissions
 * Permissions (if by username): `/login/permissions`

### Response

#### Success

On success you will get code `200` with an array of permissions as follows:
```json
{
    "permissions": [
        {
            "path": ["no_auth"],
            "auth": false
        },
        {
            "path": ["auth"],
            "auth": true
        },
        {
            "path": ["mut"],
            "auth": true,
            "mut": true
        }
    ]
}
```

More information can be found (though in TOML format) at [LrAU’s documentation](https://crates.io/crates/lrau).

#### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` with the content `INVALID_SESSION` - Indicates the user could not get their permissions as their session is invalid.
