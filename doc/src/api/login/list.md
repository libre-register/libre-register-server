# List Logins `/logins/list`

 * Method: `GET`
 * Path: `/login/list?<session>`
 * Returns: A Json Array of contacts.
 * Permission: `/login/username` (and possibly `login/uid`

## Format

The format is a http form and has the following keys:

 * `sort_asc` - (Optional) a boolean value on whether to sort ascending (true) or descending (false).
 * `offset` - (Optional, defaults to `0`) what position should the sorting be offset.
 * `limit` - (Optional, defaults to `10`) how many results should be returned.
 * `session` - The user’s session.
 * `include_uid` - Should we include the user’s uid (requires the permission `login/uid`

For example:

```sh
curl '/login/list?sort_asc=true,offset=3,limit=3,session=xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx'
```

## Response

### Success

On success the result will be a json array of objects which have a `username` and a `uid`:

```json
[
    {username: "john_doe", uid: "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx"},
    {username: "anne_parker", uid: null},
    {username: "charlie_chaplin", uid: null},
    {username: "lilly_hopper", uid: null}
]
```

### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `400` - Malformed JSON
