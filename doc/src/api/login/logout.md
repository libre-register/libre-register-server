# Logout `/login/logout/<session>`

*Where `<session>` is your login session returned from [login](login.md)*

 * Method: `DELETE`
 * Format: no content
 * Path: `/login/logout/<session>` *Where `<session>` is a valid login session.*
 * Returns: Nothing.

## Response

### Success

On success a `204` no content will be returned, indicating the session has been deleted.

### Failure

 * `401` with the content `INVALID_SESSION` - Indicates the user could not be logged out because of an invalid session.
