# Link `/login/users/<user>/uid?<session>`

 * Method: `POST` - note this can only be called once.
 * Request Format: the user’s uid.
 * Path: `/login/users/<user>/uid?<session>`
     * Where `<user>` is the username of the user who’s password will be reset.
     * Where `<session` is the currently logged in user’s session
 * Permissions: `/login/uid` with `mut` privaliges.
 * Returns: nothing (`201`)

## Request Format

The request should have a json LrAU permission. It should be the entire permissions, not a subset.

## Response

 * `201` - Success.
 * `500` - Internal server error.
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `404` - Username not found.
