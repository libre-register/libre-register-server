# Create `/login/users/`

 * Method: `POST`
 * Path: `/login/users?<session>`
 * Format: `application/json`
 * Permissions: `/login/permissions` and `/login/password` and `/login/username` all with `mut` privalliges.
 * Returns: Nothing

### Format

A JSON map with the keys:

 * `username`
 * `password`
 * `permissions` - [LrAU permissions](../../permissions/mod.md).

### Response

#### Success

On success you will get code `201` with an empty body.

#### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` - with the content `INVALID_SESSION` or `INVALID_PERMISSIONS`, pretty self explanatory.
 * `400` - Something went wrong with the JSON.
 * `409` - The user already exists.
