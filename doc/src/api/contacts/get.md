# Get contact `/contacts/<uid>?<session>`

 * Method: `GET`
 * Path: `/contacts/<uid>?<session>`
     * Where `<uid>` is the contact’s uid.
     * Where `<session>` is the user’s session.
 * Returns: `/text/vcard` - The VCard of the contact
 * Permissions: `contact/UID` for the base contact, with additional parameters required for different fields. Basically, you get the fields back which you have permission to see.

## Response

### Success

On success the response will be status code `200` and a VCard contact will be returned.

### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` with the content `INVALID_SESSION` - Indicates the user could not retireve the data as they have had a session timeout.
 * `404` - The contact cannot be found.
