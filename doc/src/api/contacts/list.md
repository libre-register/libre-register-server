# List Contacts `/contacts/list`

 * Method: `GET`
 * Path: `/contacts/list`
 * Returns: A Json Array of contacts.

## Format

The format is a http form and has the following keys:

 * `sort_asc` - (Optional) a boolean value on whether to sort ascending (true) or descending (false).
 * `include_name` - (Optional, defaults to `false`) should the user’s name be included aswell as the Uuid.
 * `offset` - (Optional, defaults to `0`) what position should the sorting be offset.
 * `limit` - (Optional, defaults to `10`) how many results should be returned.
 * `session` - The user’s session.

For example:

```sh
curl '/contacts/list?sort_asc=true,include_name=false,offset=3,limit=3,session=xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx'
```

## Response

### Success

On success the result will be a json array in the form of:

```json
[
    {
        "name": "Charlie Chaplin",
        "uid": "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx",
    },
    {
        "name": "John Doe",
        "uid": "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx",
    },
    {
        "name": "Anne Parker",
        "uid": "xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx",
    }
]
```

Note that name is just pretty formatted and is not a proper json object.

### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrive some SQL.)
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `400` - Malformed JSON
