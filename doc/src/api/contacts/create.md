# Create `/contacts/`

 * Method: `POST`
 * Path: `/contacts?<session>`
 * Format: `text/vcard`
 * Permissions: any permissions for properties you want to edit. This will mean that if inadequate permissions are given then you may end up creating a half full or empty contact entry. However, the contact will not be inserted without `/contacts/UID`.
 * Returns: Nothing

### Format

The LibreRegister [Contacts](../../formats/contacts.md) format.

### Response

#### Success

On success you will get code `201` with an empty body.

#### Failure

 * `500` - Something went wrong internally on the server (probably failed to retrieve some SQL.)
 * `401` - with the content `INVALID_SESSION` or `INVALID_PERMISSIONS`
 * `400` - Something went wrong with the JSON.
 * `409` - The contact already exists (based of the UID).
