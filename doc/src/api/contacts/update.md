# Update Contact `/contacts/<uid>?<session>`

 * Method: `PATCH`
 * Request Format: `text/vcard` the new vcard.
 * Path: `/contacts/<uid>?<session>`
     * Where `<uid>` is the contact’s uid
     * Where `<session>` is user’s session.
 * Permissions: `/contacts/UID` with `mut` privaliges. Note that for every property changed you will also need `mut` privaliges.
 * Returns: Nothing (`204`)

## Request Format

The LibreRegister [Contacts](../../formats/contacts.md) format.

## Response

 * `204` - Success.
 * `400` - VCard cannot be parsed or is not in the LibreRegister format.
 * `500` - Internal server error.
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `404` - Uid not found.
