# Delete Contact `/contacts/<uid>?<session>`

 * Method: `DELETE`
 * Path: `/contacts/<uid>?<session>`
     * Where `<uid>` is the contact’s uid
     * Where `<session>` is user’s session.
 * Permissions (nearly all of them minus `/contacts/CI/{home,work}):
    * `/contacts/UID`
    * `/contacts/N`
        * `/contacts/N/given`
        * `/contacts/N/family`
        * `/contacts/N/additional`
        * `/contacts/N/prefixes`
        * `/contacts/N/suffixes`
    * `/contacts/NICKNAME`
    * `/contacts/ANNIVERSARY`
    * `/contacts/BDAY`
    * `/contacts/PHOTO`
    * `/contacts/TITLE`
    * `/contacts/ROLE`
    * `/contacts/ORG`
        * `/contacts/ORG/org`
        * `/contacts/ORG/unit`
        * `/contacts/ORG/office`
    * `/contacts/LOGO`
    * `/contact/CI`
    * `/contacts/ADR`
        * `/contacts/ADR/work`
        * `/contacts/ADR/home`
* Returns: Nothing (`204`)

## Response

 * `204` - Success.
 * `500` - Internal server error.
 * `401` with the content `INVALID_SESSION` or `INVALID_PERMISSIONS `- Indicates the user could not retrieve the data either becaue of invalid permissions or invalid session.
 * `404` - Uid not found.
