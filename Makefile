.PHONY: fmt
PRETTIER := $(shell command -v prettier 2> /dev/null)

# Format files
fmt:
	# Format all JSON files
ifdef PRETTIER
	find . -name '*.json' -type f ! -path "./target/*" -print0 | xargs --no-run-if-empty -0 -P 0 -n 1 prettier -w
endif
	# Format all rust files.
	rustup run nightly cargo fmt
