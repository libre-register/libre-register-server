use either::Either;
use lrdb::error::SOUError as LrDBSOUError;
use rocket::form::Form;
use rocket::http::Status;
use rocket::serde::json::Json;
use sqlx::Row;

use crate::responses::{InvalidSession, SOUError};
use crate::state::State;

/// A struct which stores a username and password so that they can be
/// received from the network
#[derive(Deserialize, Serialize)]
pub struct LoginReciever<'a> {
    username: &'a str,
    password: &'a str,
}

/// Lets a user log in
#[post("/login/login", format = "application/json", data = "<login_req>")]
pub async fn login(
    state: &rocket::State<State>,
    login_req: Json<LoginReciever<'_>>,
) -> Result<(Status, String), SOUError<InvalidSession>> {
    log::info!("Recieved login request for user: {}", login_req.username);
    let login_req = login_req.into_inner();

    // Check if we have any other logins which we need to get rid of.
    state
        .logins
        .write()
        .await
        .retain(|_, v| v.username != login_req.username);

    // Try and log in
    let login = state
        .database
        .login(login_req.username, login_req.password)
        .await;

    // Gets the uuid of this login
    let uuid = uuid::Uuid::new_v4();

    match login {
        Ok(login) => {
            // Add the login to the hashmap
            state.logins.write().await.insert(uuid.to_string(), login);

            // Return the login
            Ok((Status::Created, uuid.to_string()))
        }
        Err(e) => match e {
            LrDBSOUError::User(_) => {
                log::info!("Rejecting user for incorrect credentials. {}", e);
                Err(SOUError::from(InvalidSession::invalid_session()))
            }
            LrDBSOUError::Server(e) => {
                log::error!("{} {:#?}", e, e);
                Err(SOUError::new_server())
            }
        },
    }
}

#[delete("/login/logout/<session>")]
/// Lets a user logout.
pub async fn logout(
    session: String,
    state: &rocket::State<State>,
) -> Result<Status, InvalidSession> {
    // Removes it from the hashmap
    match state.logins.write().await.remove(&session) {
        Some(s) => {
            log::info!("User logged out: {:?}", s);
            Ok(Status::NoContent)
        }
        None => Err(InvalidSession::invalid_session()),
    }
}

/// Gets a user's permissions so that a client can know what can be
/// interacted with.
#[get("/login/users/by_session/<session>/permissions")]
pub async fn get_permissions_by_session(
    session: &str,
    state: &rocket::State<State>,
) -> Result<Json<lrau::Permissions>, InvalidSession> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(session)
        .ok_or_else(InvalidSession::invalid_session)?;

    if !login.check_valid_login() {
        return Err(InvalidSession::invalid_session());
    }

    log::info!("User {} requested their permissions", login.username,);

    Ok(Json(login.permissions.clone()))
}

/// Gets a user's permissions so that a client can know what can be
/// interacted with.
#[get("/login/users/by_username/<username>/permissions?<session>")]
pub async fn get_permissions_by_username(
    username: &str,
    session: &str,
    state: &rocket::State<State>,
) -> Result<Json<lrau::Permissions>, Either<SOUError<InvalidSession>, Status>> {
    // Gets the login from the session. This is so we can vaildate that
    // we are indeed allowd to get other people's logins.
    let (logins, login_to_get) =
        tokio::join!(state.logins.read(), state.database.get_login(username),);

    let login = logins.get(session).ok_or_else(|| {
        Either::Left(SOUError::User(InvalidSession::invalid_session()))
    })?;

    if !login.check_valid_login() {
        return Err(Either::Left(SOUError::User(
            InvalidSession::invalid_session(),
        )));
    }

    log::info!(
        "User {}, is requesting the permissions of user {}.",
        login.username,
        username,
    );

    // Check for permissions.
    if !login
        .get_valid_permissions(&["login", "permissions"], false)
        .unwrap_or(false)
    {
        return Err(Either::Left(SOUError::User(
            InvalidSession::invalid_permissions(),
        )));
    }

    // Gets the login we need to get.
    let login_to_get = login_to_get.map_err(|x|
        match x {
            LrDBSOUError::User(_) => {
                log::info!(
                    "Rejecting user `{}`s attempt to get permissions of nonexistent user {}",
                    login.username,
                    username
                );
                Either::Right(Status::NotFound)
            }
            LrDBSOUError::Server(e) => {
                log::error!("{} {:#?}", e, e);
                Either::Left(SOUError::new_server())
            }
        }
    )?;

    Ok(Json(login_to_get.permissions))
}

/// Lets a user create another user.
#[post("/login/users?<session>", format = "application/json", data = "<user>")]
pub async fn create_user(
    state: &rocket::State<State>,
    user: Json<lrau::User>,
    session: &str,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Get the login.
    let logins = state.logins.read().await;
    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    // Log it
    log::info!(
        "User {}, is requesting to create a new user called {}",
        login.username,
        user.username
    );

    // Check if we have permissions.
    if login
        .get_valid_permissions(&["login", "permissions"], true)
        .unwrap_or(false) &&
        login
            .get_valid_permissions(&["login", "password"], true)
            .unwrap_or(false) &&
        login
            .get_valid_permissions(&["login", "username"], true)
            .unwrap_or(false)
    {
        // Check if this record already exists.
        if sqlx::query!(
            "SELECT FROM login_users WHERE username=$1",
            user.username
        )
        .fetch_optional(&state.database.pool)
        .await
        .map_err(|_| Either::Right(Status::InternalServerError))?
        .is_some()
        {
            return Err(Either::Right(Status::Conflict));
        }

        sqlx::query!(
            "
            INSERT INTO login_users (username, password, permissions)
            VALUES ($1, $2, $3)",
            user.username,
            user.password,
            serde_json::to_value(user.permissions.clone())
                .map_err(|_| Either::Right(Status::BadRequest))?,
        )
        .execute(&state.database.pool)
        .await
        .map_err(|_| Either::Right(Status::InternalServerError))
        .map(|_| Status::Created)
    } else {
        Err(Either::Left(InvalidSession::invalid_permissions()))
    }
}

/// Lets an admin reset the password of a user
#[patch("/login/users/<user>/password?<session>", data = "<password>")]
pub async fn reset_password(
    state: &rocket::State<State>,
    user: &str,
    password: &str,
    session: &str,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Get the login.
    let logins = state.logins.read().await;
    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    // Log it
    log::info!(
        "User {}, is requesting to change the password of a user called {}",
        login.username,
        user
    );

    // Check if we have permissions.
    if login
        .get_valid_permissions(&["login", "password"], true)
        .unwrap_or(false) ||
        login.username == user
    {
        use argon2::{Config, Variant};
        let salt = lrau::salt();
        let salt = salt.as_bytes();

        // Check if this record exists.
        if sqlx::query!("SELECT FROM login_users WHERE username=$1", user)
            .fetch_optional(&state.database.pool)
            .await
            .map_err(|_| Either::Right(Status::InternalServerError))?
            .is_none()
        {
            return Err(Either::Right(Status::NotFound));
        }

        // Gets an Argon2 config
        let config = Config {
            variant: Variant::Argon2id,
            ..Config::default()
        };

        let password = argon2::hash_encoded(password.as_bytes(), salt, &config)
            .map_err(|e| match e {
                argon2::Error::PwdTooShort | argon2::Error::PwdTooLong => {
                    Either::Right(Status::BadRequest)
                }
                _ => Either::Right(Status::InternalServerError),
            })?;

        sqlx::query!(
            "
            UPDATE login_users
            SET password = $1
            WHERE username = $2;",
            password,
            user,
        )
        .execute(&state.database.pool)
        .await
        .map_err(|_| Either::Right(Status::InternalServerError))
        .map(|_| Status::NoContent)
    } else {
        Err(Either::Left(InvalidSession::invalid_permissions()))
    }
}

/// Lets an admin modify a user's permissions.
#[patch("/login/users/<user>/permissions?<session>", data = "<permissions>")]
pub async fn reset_permissions(
    state: &rocket::State<State>,
    user: &str,
    permissions: Json<lrau::Permissions>,
    session: &str,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Get the login.
    let logins = state.logins.read().await;
    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    // Log it
    log::info!(
        "User {}, is requesting to change the password of a user called {}",
        login.username,
        user
    );

    // Check if we have permissions.
    if login
        .get_valid_permissions(&["login", "permissions"], true)
        .unwrap_or(false)
    {
        // Check if this record exists.
        if sqlx::query!("SELECT FROM login_users WHERE username=$1", user)
            .fetch_optional(&state.database.pool)
            .await
            .map_err(|_| Either::Right(Status::InternalServerError))?
            .is_none()
        {
            return Err(Either::Right(Status::NotFound));
        }

        sqlx::query!(
            "
            UPDATE login_users
            SET permissions = $1
            WHERE username = $2;",
            serde_json::to_value(permissions.into_inner())
                .map_err(|_| Either::Right(Status::BadRequest))?,
            user,
        )
        .execute(&state.database.pool)
        .await
        .map_err(|_| Either::Right(Status::InternalServerError))
        .map(|_| Status::NoContent)
    } else {
        Err(Either::Left(InvalidSession::invalid_permissions()))
    }
}

#[derive(Serialize, Deserialize, FromForm)]
pub struct LoginLister<'v> {
    /// Are we sorting descending (`Some(false)`) or ascending
    /// (`Some(true)`) or neither (`None`)
    #[field(default_with = None)]
    pub sort_asc: Option<bool>,

    /// Start (for querying fields based of scrolling)
    #[field(default = 0)]
    pub offset: u64,

    /// Count
    #[field(default = 10)]
    pub limit: u64,

    /// Session
    pub session: &'v str,

    /// Should include the uid as well as the username?.
    #[field(default_with = Some(false))]
    pub include_uid: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LoginListerResponseObject {
    pub username: String,
    pub uid: Option<String>,
}

/// Gets a list of all the logins.
#[get("/login/list", data = "<info>")]
pub async fn get_login_listing(
    state: &rocket::State<State>,
    info: Form<LoginLister<'_>>,
) -> Result<Json<Vec<LoginListerResponseObject>>, Either<InvalidSession, Status>>
{
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(info.session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    if !(login
        .get_valid_permissions(&["login", "username"], false)
        .unwrap_or(false) &&
        (!info.include_uid ||
            login
                .get_valid_permissions(&["login", "uid"], false)
                .unwrap_or(false)))
    {
        return Err(Either::Left(InvalidSession::invalid_permissions()));
    }

    // Generate some SQL.
    let mut sql;
    if info.include_uid {
        sql = String::from("SELECT username,uid FROM login_users");
    } else {
        sql = String::from("SELECT username from login_users");
    }

    // ORDER BY
    match info.sort_asc {
        Some(true) => sql.push_str(" ORDER BY username ASC"),
        Some(false) => sql.push_str(" ORDER BY username DESC"),
        None => (),
    }

    // LIMIT
    sql.push_str(&format!(" LIMIT {} OFFSET {};", info.limit, info.offset));

    let query = sqlx::query(&sql)
        .fetch_all(&state.database.pool)
        .await
        .map_err(|e| {
            log::error!("{}", e);
            Either::Right(Status::InternalServerError)
        })?;

    // Create the final value.
    let vals: Result<Vec<LoginListerResponseObject>, Either<_, Status>> = query
        .into_iter()
        .map(|x| {
            Ok(LoginListerResponseObject {
                username: x.try_get("username").map_err(|e| {
                    log::error!("{}", e);
                    Either::Right(Status::InternalServerError)
                })?,
                uid: x.try_get("uid").ok(),
            })
        })
        .collect();

    Ok(Json(vals?))
}

#[delete("/login/users/<username>?<session>")]
pub async fn delete_user(
    session: &str,
    username: &str,
    state: &rocket::State<State>,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    // Check if we have permissions.
    if !login
        .get_valid_permissions(&["login", "permissions"], true)
        .unwrap_or(false) &&
        login
            .get_valid_permissions(&["login", "password"], true)
            .unwrap_or(false) &&
        login
            .get_valid_permissions(&["login", "username"], true)
            .unwrap_or(false)
    {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    let record =
        sqlx::query!("SELECT FROM login_users WHERE username=$1", username)
            .fetch_optional(&state.database.pool)
            .await
            .map_err(|e| {
                log::error!("{}", e);
                Either::Right(Status::InternalServerError)
            })?;

    // Check if this record exists.
    if record.is_none() {
        log::trace!("User not found {}", username);
        return Err(Either::Right(Status::NotFound));
    }

    sqlx::query!(
        r#"
        DELETE FROM login_users
        WHERE username = $1
        "#,
        username
    )
    .execute(&state.database.pool)
    .await
    .map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    drop(logins);

    state
        .logins
        .write()
        .await
        .retain(|_session, value| value.username != username);

    Ok(Status::NoContent)
}

/// Lets one assign a uid to a login_user.
#[post("/login/users/<user>/uid?<session>", data = "<uid>")]
pub async fn assign_uid(
    state: &rocket::State<State>,
    uid: &str,
    user: &str,
    session: &str,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Gets the login
    let logins = state.logins.read().await;
    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    log::info!(
        "Assigning uid `{}` to user {} as requested by {}.",
        uid,
        user,
        login.username
    );

    // Check if we have permissions
    if login
        .get_valid_permissions(&["login", "uid"], true)
        .unwrap_or(false)
    {
        // Check if this record exists and has not a uid.
        if sqlx::query!("SELECT uid FROM login_users WHERE username=$1", user)
            .fetch_optional(&state.database.pool)
            .await
            .map_err(|_| Either::Right(Status::InternalServerError))?
            .ok_or(Either::Right(Status::NotFound))?
            .uid
            .is_some()
        {
            return Err(Either::Right(Status::Conflict));
        }

        sqlx::query!(
            "
            UPDATE login_users
            SET uid = $1
            WHERE username=$2
            ",
            uid,
            user
        )
        .execute(&state.database.pool)
        .await
        .map_err(|e| {
            log::error!("{}", e);
            Either::Right(Status::InternalServerError)
        })?;

        Ok(Status::Created)
    } else {
        Err(Either::Left(InvalidSession::invalid_permissions()))
    }
}

/// Gets a user's uid
#[get("/login/users/<username>/uid?<session>")]
pub async fn get_uid(
    username: &str,
    session: &str,
    state: &rocket::State<State>,
) -> Result<(Status, String), Either<InvalidSession, Status>> {
    // Gets the login from the session.
    let logins = state.logins.read().await;

    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    log::info!(
        "User {}, is requesting the uid of user {}.",
        login.username,
        username,
    );

    // Check for permissions.
    if !(login
        .get_valid_permissions(&["login", "uid"], false)
        .unwrap_or(false) ||
        username == login.username)
    {
        return Err(Either::Left(InvalidSession::invalid_permissions()));
    }

    // Gets the login we need to get.
    Ok(sqlx::query!(
        "SELECT uid
        FROM login_users
        WHERE username = $1",
        username,
    )
    .fetch_optional(&state.database.pool)
    .await
    .map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?
    .ok_or(Either::Right(Status::NotFound))?
    .uid
    .map(|x| (Status::Ok, x))
    .unwrap_or((Status::NoContent, "".to_string())))
}
