/// Generates an SQL statement based of LrAU permissions.
#[macro_export]
macro_rules! lrau_sql_select {
    ($table:expr, $user:expr, {$($permission:expr, $mutable:expr => $column:expr),* $(,)?} ) => {
        {
            let mut sql = String::from("SELECT ");
            $(
                // Adds in all the column names.
                if lrau::User::get_valid_permissions($user, $permission, $mutable).unwrap_or(false) {
                    sql.push_str($column);
                    sql.push(',');
                }
            )*

            // Removes a trailing comma.
            if sql.chars().last() == Some(',') {
                sql.pop();
            }


            sql.push_str(" FROM ");
            sql.push_str($table);

            sql
        }
    };
    ($table:expr, $user:expr, {$($permission:expr => $column:expr),* $(,)?} ) => {
        {
            lrau_sql_select!(
                $table,
                $user,
                {$(
                    $permission, false => $column,
                )*}
            )
        }
    };
}

/// Generates an SQL update statement based of LrAU permissions.
#[macro_export]
macro_rules! lrau_sql_update {
    ($table:expr, $user:expr, $condition:expr, {$($permission:expr, $mutable:expr => $column:expr),* $(,)?} ) => {
        {
            let mut sql = String::from("UPDATE ");
            sql.push_str($table);
            sql.push_str(" SET ");

            let mut n = 1;
            $(
                // Adds in all the column names.
                if lrau::User::get_valid_permissions(
                    $user,
                    $permission,
                    $mutable
                ).unwrap_or(false) {
                    sql.push_str(&format!("{} = ${},", $column, n));
                    n += 1;
                }
            )*

            // Removes a trailing comma.
            if sql.chars().last() == Some(',') {
                sql.pop();
            }

            sql.push(' ');
            sql.push_str($condition);

            sql
        }
    };
    ($table:expr, $user:expr, $condition:expr, {$($permission:expr => $column:expr),* $(,)?} ) => {
        {
            lrau_sql_update!(
                $table,
                $user,
                $condition,
                {$(
                    $permission, true => $column,
                )*}
            )
        }
    };
}

/// Generates an SQL insert into statement based of LrAU permissions.
#[macro_export]
macro_rules! lrau_sql_insert {
    ($table:expr, $user:expr, {$($permission:expr, $mutable:expr => $column:expr),* $(,)?} ) => {
        {
            let mut sql_column_names = format!(
                "INSERT INTO {} (",
                $table
            );
            let mut sql_values = String::from("VALUES (");

            let mut n = 1;
            $(
                // Adds in all the column names.
                if lrau::User::get_valid_permissions(
                    $user,
                    $permission,
                    $mutable
                ).unwrap_or(false) {
                    sql_column_names.push_str($column);
                    sql_values.push('$');
                    sql_values.push_str(&n.to_string());
                    sql_values.push(',');
                    n += 1;
                }
            )*

            // Removes a trailing comma.
            if sql_column_names.chars().last() == Some(',') {
                sql_column_names.pop();
            }

            if sql_values.chars().last() == Some(',') {
                sql_values.pop();
            }

            // Add some closing brackets
            sql_column_names.push(')');
            sql_values.push(')');

            // Merge the two values
            let mut sql = sql_column_names;
            sql.push(' ');
            sql.push_str(&sql_values);

            sql
        }
    };
    ($table:expr, $user:expr, $condition:expr, {$($permission:expr => $column:expr),* $(,)?} ) => {
        {
            lrau_sql_insert!(
                $table,
                $user,
                {$(
                    $permission, true => $column,
                )*}
            )
        }
    };
}

#[macro_export]
macro_rules! lrau_sql_bind {
    ($query:expr, $user:expr, {$($permission:expr, $mutable:literal => $val:expr),* $(,)?} ) => {{
        let mut query = $query;

        $(
            if lrau::User::get_valid_permissions(
                $user,
                $permission,
                $mutable
            ).unwrap_or(false) {
                query = query.bind($val);
            }
        )*

        query
    }};
}

#[macro_export]
macro_rules! sql_get {
    (
        $query:expr,
        $typ:ident,
        { $( $pre_name:ident : $pre_val:expr),* $(,)?},
        { $( $name:ident ),* $(,)?}
    ) => {
        {
            $typ {
                $(
                    $pre_name: $pre_val,
                )*
                $(
                    $name: $query.try_get(stringify!($name)).unwrap_or_default(),
                )*
            }
        }
    };
    (
        $query:expr,
        $typ:ident,
        { $( $pre_name:ident : $pre_val:expr),* $(,)?},
        { $( $name:literal => $name_val:ident ),* $(,)?}
    ) => {
        {
            $typ {
                $(
                    $pre_name: $pre_val,
                )*
                $(
                    $name_val: $query.try_get($name).unwrap_or_default(),
                )*
            }
        }
    }
}
