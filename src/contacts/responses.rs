#[derive(Responder)]
pub enum ContactResponder {
    #[response(status = 200, content_type = "text/vcard")]
    VCard(String),
    #[response(status = 404)]
    NotFound(()),
    #[response(status = 500)]
    ServerError(()),
    #[response(status = 401)]
    InvalidSession(crate::responses::session::InvalidSession),
}
