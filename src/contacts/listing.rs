//! # Listing
//!
//! This contains information on how to list contacts and query them.
use contack::Name;
use either::Either::{self, Left, Right};
use rocket::form::Form;
use rocket::serde::json::Json;
use sqlx::Row;

use crate::responses::InvalidSession;
use crate::responses::ServerError;
use crate::sql::lrau_sql_select;
use crate::sql_get;
use crate::state::State;

#[derive(Serialize, Deserialize, FromForm)]
pub struct ContactLister<'v> {
    /// Are we sorting descending (`Some(false)`) or ascending
    /// (`Some(true)`) or neither (`None`)
    #[field(default_with = None)]
    pub sort_asc: Option<bool>,

    /// Should include the name as well as the uuid.
    #[field(default_with = Some(false))]
    pub include_name: bool,

    /// Start (for querying fields based of scrolling)
    #[field(default = 0)]
    pub offset: u64,

    /// Count
    #[field(default = 10)]
    pub limit: u64,

    /// Session
    pub session: &'v str,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ContactListerResponseObject {
    pub name: Option<String>,
    pub uid: String,
}

/// Gets a list of all contacts.
#[get("/contacts/list", data = "<info>")]
pub async fn get_contact_listing(
    state: &rocket::State<State>,
    info: Form<ContactLister<'_>>,
) -> Result<
    Json<Vec<ContactListerResponseObject>>,
    Either<InvalidSession, ServerError>,
> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(info.session)
        .ok_or_else(|| Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Left(InvalidSession::invalid_session()));
    }

    // Generate some SQL.
    let mut sql;

    // SELECT
    if info.include_name {
        sql = lrau_sql_select(
            "contacts",
            login,
            &super::CONTACT_LRAU_SQL_PERMISSIONS[..6],
        );
    } else {
        sql = String::from("SELECT uid ");
    }

    // ORDER BY
    match info.sort_asc {
        Some(true) => sql.push_str(" ORDER BY name_family ASC"),
        Some(false) => sql.push_str(" ORDER BY name_family DESC"),
        None => (),
    }

    // LIMIT
    sql.push_str(&format!(" LIMIT {} OFFSET {};", info.limit, info.offset));

    let query = sqlx::query(&sql)
        .fetch_all(&state.database.pool)
        .await
        .map_err(|_| Right(ServerError(())))?;

    // Create the names.
    let vals = query.into_iter().map(|x| ContactListerResponseObject {
        name: if info.include_name {
            Some(
                sql_get! {
                    x,
                    Name,
                    {},
                    {
                        // Name
                        "name_given" => given,
                        "name_additional" => additional,
                        "name_family" => family,
                        "name_prefixes" => prefixes,
                        "name_suffixes" => suffixes,
                    }
                }
                .to_string(),
            )
        } else {
            None
        },
        uid: x.try_get("uid").unwrap_or_default(),
    });
    let vals = vals.collect();

    Ok(Json(vals))
}
