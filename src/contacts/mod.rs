pub mod responses;
use std::convert::TryInto;
pub mod listing;

use contack::{
    read_write::vcard::VCard,
    sql::{contact_information::SqlContactInformation, SqlContact},
    Contact, ContactInformation,
};
use either::Either;
use responses::ContactResponder;
use rocket::http::Status;
use sqlx::Row;

use crate::responses::InvalidSession;
use crate::sql::*;
use crate::state::State;
use crate::{lrau_sql_bind, sql_get};

lazy_static! {
    pub static ref CONTACT_LRAU_SQL_PERMISSIONS: &'static [LrAUSqlCorrelation<'static, 'static, 'static>] = &[
        // Uuid
        LrAUSqlCorrelation {perm: &["contacts", "UID"], col: "uid"},

        // Name Fields
        LrAUSqlCorrelation {perm: &["contacts", "N", "given"], col: "name_given"},
        LrAUSqlCorrelation {perm: &["contacts", "N", "family"], col: "name_family"},
        LrAUSqlCorrelation {perm: &["contacts", "N", "additional"], col: "name_additional"},
        LrAUSqlCorrelation {perm: &["contacts", "N", "prefixes"], col: "name_prefixes"},
        LrAUSqlCorrelation {perm: &["contacts", "N", "suffixes"], col: "name_suffixes"},

        // Nickname
        LrAUSqlCorrelation {perm: &["contacts", "NICKNAME"], col: "nickname"},

        // Anniversary
        LrAUSqlCorrelation {perm: &["contacts", "ANNIVERSARY"], col: "anniversary"},

        // Bday
        LrAUSqlCorrelation {perm: &["contacts", "BDAY"], col: "bday"},

        // Photo
        LrAUSqlCorrelation {perm: &["contacts", "PHOTO"], col: "photo_uri"},
        LrAUSqlCorrelation {perm: &["contacts", "PHOTO"], col: "photo_bin"},
        LrAUSqlCorrelation {perm: &["contacts", "PHOTO"], col: "photo_mime"},

        // Title
        LrAUSqlCorrelation {perm: &["contacts", "TITLE"], col: "title"},

        // Role
        LrAUSqlCorrelation {perm: &["contacts", "ROLE"], col: "role"},

        // Org
        LrAUSqlCorrelation {perm: &["contacts", "ORG", "org"], col: "org_org"},
        LrAUSqlCorrelation {perm: &["contacts", "ORG", "unit"], col: "org_unit"},
        LrAUSqlCorrelation {perm: &["contacts", "ORG", "office"], col: "org_office"},

        // Logo
        LrAUSqlCorrelation {perm: &["contacts", "LOGO"], col: "logo_uri"},
        LrAUSqlCorrelation {perm: &["contacts", "LOGO"], col: "logo_bin"},
        LrAUSqlCorrelation {perm: &["contacts", "LOGO"], col: "logo_mime"},

        // Address
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_street"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_locality"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_region"},

        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_code"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_country"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_geo_longitude"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "home"], col: "home_address_geo_latitude"},

        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_street"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_locality"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_region"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_code"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_country"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_geo_longitude"},
        LrAUSqlCorrelation {perm: &["contacts", "ADR", "work"], col: "work_address_geo_latitude"},
    ];
}

#[must_use]
pub fn get_contact_sql(login: &lrau::User) -> String {
    // Begin the sql statement
    let mut sql =
        lrau_sql_select("contacts", login, &CONTACT_LRAU_SQL_PERMISSIONS);

    // Make sure we only select the contact with the correct uid.
    sql.push_str(" WHERE uid=$1;");

    sql
}

/// Gets a contact's contact information.
///
/// # Panics
///
/// Panics at compile time if the SQL is fudged up.
///
/// # Errors
///
/// Returns an error in the case of failed SQL.
pub async fn get_contact_ci(
    uid: &str,
    login: &lrau::User,
    state: &State,
) -> Result<Vec<SqlContactInformation>, ContactResponder> {
    // Now we shall attempt some contact informations.
    if login
        .get_valid_permissions(&["contacts", "CI"], false)
        .unwrap_or(false)
    {
        // Retrieve it from the database.
        let mut ci: Vec<SqlContactInformation> = sqlx::query_as!(
            SqlContactInformation,
            r#"
                SELECT
                    pid,
                    pref,
                    value,
                    platform as "platform: contack::ContactPlatform",
                    typ as "typ: contack::Type",
                    uid
                FROM contacts_contact_information
                WHERE
                    uid = $1;
            "#,
            uid,
        )
        .fetch_all(&state.database.pool)
        .await
        .map_err(|_| ContactResponder::ServerError(()))?;

        // Remove all the one's we don't have permission to see.
        ci.retain(|x| {
            login
                .get_valid_permissions(&["contacts", "CI", "home"], false)
                .unwrap_or_default() ||
                x.typ == None ||
                x.typ == Some(contack::Type::Work)
        });

        ci.retain(|x| {
            login
                .get_valid_permissions(&["contacts", "CI", "work"], false)
                .unwrap_or_default() ||
                x.typ == None ||
                x.typ == Some(contack::Type::Home)
        });

        Ok(ci)
    } else {
        Ok(Vec::new())
    }
}

/// Lets a user get a contact.
///
/// Note that it strips out fields you don't have the permissions for.
///
/// # Errors
///
/// Returns an error if it cannot access the SQL or if the user has
/// insufficient permissions.
#[get("/contacts/<uid>?<session>", format = "text/vcard")]
pub async fn get_contact(
    session: &str,
    uid: &str,
    state: &rocket::State<State>,
) -> Result<ContactResponder, ContactResponder> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins.get(session).ok_or_else(|| {
        ContactResponder::InvalidSession(InvalidSession::invalid_session())
    })?;

    if !login.check_valid_login() {
        return Err(ContactResponder::InvalidSession(
            InvalidSession::invalid_session(),
        ));
    }

    if !login
        .get_valid_permissions(&["contacts", "UID"], false)
        .unwrap_or(false)
    {
        return Err(ContactResponder::InvalidSession(
            InvalidSession::invalid_permissions(),
        ));
    }

    log::info!(
        "User {} requested the contact with the uid: {}",
        login.username,
        uid,
    );

    let sql = get_contact_sql(login);
    let (query, ci) = tokio::try_join!(
        async {
            sqlx::query(&sql)
                .bind(&uid)
                .fetch_one(&state.database.pool)
                .await
                .map_err(|e| {
                    if let sqlx::Error::RowNotFound = e {
                        log::info!(
                            "User {} failed to fetch contact {}",
                            session,
                            uid
                        );
                        ContactResponder::NotFound(())
                    } else {
                        log::error!(
                            "When trying to fetch contact {}: {} {:#?}",
                            uid,
                            e,
                            e
                        );
                        ContactResponder::ServerError(())
                    }
                })
        },
        get_contact_ci(uid, login, state)
    )?;

    // Create the raw contact. This, unforunately, must still be done through
    // a macro so no reusable code here.
    let raw_contact = sql_get! {
        query,
        SqlContact,
        {
            // Uuid
            uid: uid.to_string(),
        },
        {
            // Name
            name_given,
            name_additional,
            name_family,
            name_prefixes,
            name_suffixes,

            // Nickname
            nickname,

            // Anniversary
            anniversary,

            // Birthday
            bday,

            // Photo
            photo_uri,
            photo_bin,
            photo_mime,

            // Title
            title,

            // Role
            role,

            // Org
            org_org,
            org_unit,
            org_office,

            // Logo
            logo_uri,
            logo_bin,
            logo_mime,

            home_address_street,
            home_address_locality,
            home_address_region,
            home_address_code,
            home_address_country,
            home_address_geo_longitude,
            home_address_geo_latitude,

            work_address_street,
            work_address_locality,
            work_address_region,
            work_address_code,
            work_address_country,
            work_address_geo_longitude,
            work_address_geo_latitude
        }
    };

    // Attempt to convert it to a normal contact.
    let mut contact: Contact = raw_contact
        .try_into()
        .map_err(|_| ContactResponder::ServerError(()))?;

    let ci = ci.into_iter().map(ContactInformation::from);
    contact.contact_information.extend(ci);

    // Convert it to a vcard.
    let vcard: VCard = contact.into();

    Ok(ContactResponder::VCard(vcard.to_string()))
}

#[post("/contacts?<session>", format = "text/vcard", data = "<contact>")]
pub async fn create_contact(
    session: &str,
    contact: &str,
    state: &rocket::State<State>,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    // Parse the contact
    let vcard: VCard = contact.parse().map_err(|e| {
        log::trace!("Error when creating contacts (VCard stage):\n{}", e);
        Either::Right(Status::BadRequest)
    })?;
    let mut contact: Contact = vcard.try_into().map_err(|e| {
        log::trace!("Error when creating contacts (contack stage):\n{}", e);
        Either::Right(Status::BadRequest)
    })?;

    log::info!(
        "User {} is attempting to insert a contact with the name {}, and the uid of {}",
        login.username,
        contact.name,
        contact.uid,
    );

    // Checks if we have permissions to insert a contact.
    if !login
        .get_valid_permissions(&["contacts", "UID"], true)
        .unwrap_or(false)
    {
        log::trace!(
            "User {} does not have permission to insert a contact",
            login.username
        );
        return Err(Either::Left(InvalidSession::invalid_permissions()));
    }

    // Check if this record already exists.
    if sqlx::query!("SELECT FROM contacts WHERE uid=$1", contact.uid)
        .fetch_optional(&state.database.pool)
        .await
        .map_err(|_| Either::Right(Status::InternalServerError))?
        .is_some()
    {
        log::trace!("Conflict over contact {}", contact.uid);
        return Err(Either::Right(Status::Conflict));
    }

    // Extract the contact's contact information.
    let ci = SqlContactInformation::extract_contact_information(&mut contact);

    // Convert the contact to something that
    // can be stored in a relational database.
    let contact: SqlContact = contact.try_into().map_err(|e| {
        log::trace!("Error when creating contacts (contack stage):\n{}", e);
        Either::Right(Status::BadRequest)
    })?;

    // Create the SQL needed to insert this contact.
    let sql = lrau_sql_insert("contacts", login, &CONTACT_LRAU_SQL_PERMISSIONS);

    let query = sqlx::query(&sql);

    // Bind it (put all the values in the $Ns)
    let query = lrau_sql_bind!(
        query,
        login,
        {
            &["contacts", "UID"], true => contact.uid.clone(),

            // Name Fields
            &["contacts", "N", "given"], true => contact.name_given,
            &["contacts", "N", "family"], true => contact.name_family,
            &["contacts", "N", "additional"], true => contact.name_additional,
            &["contacts", "N", "prefixes"], true => contact.name_prefixes,
            &["contacts", "N", "suffixes"], true => contact.name_suffixes,

            // Nickname
            &["contacts", "NICKNAME"], true => contact.nickname,

            // Anniversary
            &["contacts", "ANNIVERSARY"], true => contact.anniversary,

            // Bday
            &["contacts", "BDAY"], true => contact.bday,

            // Photo
            &["contacts", "PHOTO"], true => contact.photo_uri,
            &["contacts", "PHOTO"], true => contact.photo_bin,
            &["contacts", "PHOTO"], true => contact.photo_mime,

            // Title
            &["contacts", "TITLE"], true => contact.title,

            // Role
            &["contacts", "ROLE"], true => contact.role,

            // Org
            &["contacts", "ORG", "org"], true => contact.org_org,
            &["contacts", "ORG", "unit"], true => contact.org_unit,
            &["contacts", "ORG", "office"], true => contact.org_office,

            // Logo
            &["contacts", "LOGO"], true => contact.logo_uri,
            &["contacts", "LOGO"], true => contact.logo_bin,
            &["contacts", "LOGO"], true => contact.logo_mime,

            // Address
            &["contacts", "ADR", "home"], true => contact.home_address_street,
            &["contacts", "ADR", "home"], true => contact.home_address_locality,
            &["contacts", "ADR", "home"], true => contact.home_address_region,

            &["contacts", "ADR", "home"], true => contact.home_address_code,
            &["contacts", "ADR", "home"], true => contact.home_address_country,
            &["contacts", "ADR", "home"], true => contact.home_address_geo_longitude,
            &["contacts", "ADR", "home"], true => contact.home_address_geo_latitude,

            &["contacts", "ADR", "work"], true => contact.work_address_street,
            &["contacts", "ADR", "work"], true => contact.work_address_locality,
            &["contacts", "ADR", "work"], true => contact.work_address_region,
            &["contacts", "ADR", "work"], true => contact.work_address_code,
            &["contacts", "ADR", "work"], true => contact.work_address_country,
            &["contacts", "ADR", "work"], true => contact.work_address_geo_longitude,
            &["contacts", "ADR", "work"], true => contact.work_address_geo_latitude,
        }
    );

    // Create a database transaction
    let mut transaction = state.database.pool.begin().await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    // Check if we have permission to insert the conact information
    if login
        .get_valid_permissions(&["contacts", "CI"], true)
        .unwrap_or(false)
    {
        remove_ci(contact.uid.clone(), ci, &mut transaction)
            .await
            .map_err(|e| {
                log::error!("{}", e);
                Either::Right(Status::InternalServerError)
            })?;
    }

    query.execute(&mut transaction).await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    transaction.commit().await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    Ok(Status::Created)
}

#[patch("/contacts/<uid>?<session>", format = "text/vcard", data = "<contact>")]
pub async fn update_contact(
    session: &str,
    uid: &str,
    contact: &str,
    state: &rocket::State<State>,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    // Parse the contact
    let vcard: VCard = contact.parse().map_err(|e| {
        log::trace!("Error when updating contacts (VCard stage):\n{}", e);
        Either::Right(Status::BadRequest)
    })?;
    let mut contact: Contact = vcard.try_into().map_err(|e| {
        log::trace!("Error when updating contacts (contack stage):\n{}", e);
        Either::Right(Status::BadRequest)
    })?;

    log::info!(
        "User {} is attempting to update a contact with the name {}, and the uid of {}",
        login.username,
        contact.name,
        contact.uid,
    );

    // Checks if we have permissions to update a contact.
    if !login
        .get_valid_permissions(&["contacts", "UID"], true)
        .unwrap_or(false)
    {
        log::trace!(
            "User {} does not have permission to update a contact",
            login.username
        );
        return Err(Either::Left(InvalidSession::invalid_permissions()));
    }

    let record = sqlx::query!("SELECT FROM contacts WHERE uid=$1", contact.uid)
        .fetch_optional(&state.database.pool)
        .await
        .map_err(|e| {
            log::error!("{}", e);
            Either::Right(Status::InternalServerError)
        })?;

    // Check if this record already exists.
    if record.is_none() {
        log::trace!("Contact not found {}", contact.uid);
        return Err(Either::Right(Status::NotFound));
    }

    // Extract the contact's contact information.
    let ci = SqlContactInformation::extract_contact_information(&mut contact);

    // Convert the contact to something that
    // can be stored in a relational database.
    let contact: SqlContact = contact.try_into().map_err(|e| {
        log::trace!("Error when creating contacts (contack stage):\n{}", e);
        Either::Right(Status::BadRequest)
    })?;

    // Create the SQL needed to insert this contact.
    let sql = lrau_sql_update(
        "contacts",
        login,
        "WHERE uid = $1",
        1,
        &CONTACT_LRAU_SQL_PERMISSIONS[1..],
    );

    let query = sqlx::query(&sql);
    let query = query.bind(&uid);

    // Bind it (put all the values in the $Ns)
    let query = lrau_sql_bind!(
        query,
        login,
        {

            // Name Fields
            &["contacts", "N", "given"], true => contact.name_given,
            &["contacts", "N", "family"], true => contact.name_family,
            &["contacts", "N", "additional"], true => contact.name_additional,
            &["contacts", "N", "prefixes"], true => contact.name_prefixes,
            &["contacts", "N", "suffixes"], true => contact.name_suffixes,

            // Nickname
            &["contacts", "NICKNAME"], true => contact.nickname,

            // Anniversary
            &["contacts", "ANNIVERSARY"], true => contact.anniversary,

            // Bday
            &["contacts", "BDAY"], true => contact.bday,

            // Photo
            &["contacts", "PHOTO"], true => contact.photo_uri,
            &["contacts", "PHOTO"], true => contact.photo_bin,
            &["contacts", "PHOTO"], true => contact.photo_mime,

            // Title
            &["contacts", "TITLE"], true => contact.title,

            // Role
            &["contacts", "ROLE"], true => contact.role,

            // Org
            &["contacts", "ORG", "org"], true => contact.org_org,
            &["contacts", "ORG", "unit"], true => contact.org_unit,
            &["contacts", "ORG", "office"], true => contact.org_office,

            // Logo
            &["contacts", "LOGO"], true => contact.logo_uri,
            &["contacts", "LOGO"], true => contact.logo_bin,
            &["contacts", "LOGO"], true => contact.logo_mime,

            // Address
            &["contacts", "ADR", "home"], true => contact.home_address_street,
            &["contacts", "ADR", "home"], true => contact.home_address_locality,
            &["contacts", "ADR", "home"], true => contact.home_address_region,

            &["contacts", "ADR", "home"], true => contact.home_address_code,
            &["contacts", "ADR", "home"], true => contact.home_address_country,
            &["contacts", "ADR", "home"], true => contact.home_address_geo_longitude,
            &["contacts", "ADR", "home"], true => contact.home_address_geo_latitude,

            &["contacts", "ADR", "work"], true => contact.work_address_street,
            &["contacts", "ADR", "work"], true => contact.work_address_locality,
            &["contacts", "ADR", "work"], true => contact.work_address_region,
            &["contacts", "ADR", "work"], true => contact.work_address_code,
            &["contacts", "ADR", "work"], true => contact.work_address_country,
            &["contacts", "ADR", "work"], true => contact.work_address_geo_longitude,
            &["contacts", "ADR", "work"], true => contact.work_address_geo_latitude,
        }
    );

    // Create a database transaction
    let mut transaction = state.database.pool.begin().await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    // Check if we have permission to insert the conact information
    if login
        .get_valid_permissions(&["contacts", "CI"], true)
        .unwrap_or(false)
    {
        remove_ci(contact.uid.clone(), ci, &mut transaction)
            .await
            .map_err(|e| {
                log::error!("{}", e);
                Either::Right(Status::InternalServerError)
            })?;
    }

    query.execute(&state.database.pool).await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    Ok(Status::NoContent)
}

async fn remove_ci(
    uid: String,
    ci: Vec<SqlContactInformation>,
    db: &mut sqlx::Transaction<'_, sqlx::Postgres>,
) -> Result<(), sqlx::Error> {
    sqlx::query!("DELETE FROM contacts_contact_information WHERE uid=$1", uid)
        .execute(&mut *db)
        .await?;
    for ci in ci {
        sqlx::query(
            r#"
            INSERT INTO contacts_contact_information
            VALUES (
               $1,
               $2,
               $3,
               $4,
               $5,
               $6
            );
        "#,
        )
        .bind(&ci.pid)
        .bind(&ci.pref)
        .bind(&ci.value)
        .bind(&ci.platform)
        .bind(&ci.typ)
        .bind(&ci.uid)
        .execute(&mut *db)
        .await?;
    }
    Ok(())
}

#[delete("/contacts/<uid>?<session>")]
pub async fn delete_contact(
    session: &str,
    uid: &str,
    state: &rocket::State<State>,
) -> Result<Status, Either<InvalidSession, Status>> {
    // Gets the login
    let logins = state.logins.read().await;

    let login = logins
        .get(session)
        .ok_or_else(|| Either::Left(InvalidSession::invalid_session()))?;

    if !login.check_valid_login() {
        return Err(Either::Left(InvalidSession::invalid_session()));
    }

    let mut has_permissions = true;
    for i in CONTACT_LRAU_SQL_PERMISSIONS.iter() {
        has_permissions &=
            login.get_valid_permissions(i.perm, true).unwrap_or(false);
    }
    has_permissions &= login
        .get_valid_permissions(&["contacts", "CI"], true)
        .unwrap_or(false);

    if !has_permissions {
        return Err(Either::Left(InvalidSession::invalid_permissions()));
    }

    let record = sqlx::query!("SELECT FROM contacts WHERE uid=$1", uid)
        .fetch_optional(&state.database.pool)
        .await
        .map_err(|e| {
            log::error!("{}", e);
            Either::Right(Status::InternalServerError)
        })?;

    // Check if this record exists.
    if record.is_none() {
        log::trace!("Contact not found {}", uid);
        return Err(Either::Right(Status::NotFound));
    }

    let mut transaction = state.database.pool.begin().await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;
    sqlx::query!(
        r#"
        DELETE FROM contacts
        WHERE uid = $1
        "#,
        uid
    )
    .execute(&mut transaction)
    .await
    .map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    sqlx::query!(
        r#"
        DELETE FROM contacts_contact_information
        WHERE uid = $1
        "#,
        uid
    )
    .execute(&mut transaction)
    .await
    .map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    transaction.commit().await.map_err(|e| {
        log::error!("{}", e);
        Either::Right(Status::InternalServerError)
    })?;

    Ok(Status::NoContent)
}
