use serde::Deserialize;
use warp::Filter;

pub fn post_json<'de, T: Send + Sync + for<'de2> Deserialize<'de2>>() -> impl Filter<Extract = (T,), Error = warp::Rejection> + Clone {
    // When accepting a body, we want a JSON body
    // (and to reject huge payloads)...
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}
