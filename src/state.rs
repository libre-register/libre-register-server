use std::collections::HashMap;
use std::env;
use std::sync::Arc;

use lrdb::database::Database;
use tokio::sync::RwLock;

#[derive(Clone)]
pub struct State {
    pub logins: Arc<RwLock<HashMap<String, lrau::User>>>,
    pub database: Arc<Database>,
}

impl State {
    /// Creates a new database and state.
    /// Note that this will panic if the Database cannot be connected to.
    pub async fn new() -> State {
        Self {
            logins: Arc::new(RwLock::new(HashMap::new())),
            database: Arc::new(
                Database::new(
                    &env::var("DATABASE_URL")
                        .expect("DATABASE_URL MUST BE SET"),
                )
                .await
                .expect("ERROR WHEN CONNECTING TO DATABASE."),
            ),
        }
    }
}
