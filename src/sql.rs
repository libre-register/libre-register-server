use lrau::User;

pub struct LrAUSqlCorrelation<'a, 'b, 'c> {
    /// The permission required to activated the sql column.
    pub perm: &'a [&'b str],
    /// The sql column that will be activated.
    pub col: &'c str,
}

/// Generates an SQL statement based of LrAU permissions.
#[must_use]
pub fn lrau_sql_select(
    table: &str,
    user: &User,
    correlations: &[LrAUSqlCorrelation],
) -> String {
    let mut sql = String::from("SELECT ");
    for cor in correlations {
        // Adds in all the column names.
        if lrau::User::get_valid_permissions(user, cor.perm, false)
            .unwrap_or(false)
        {
            sql.push_str(cor.col);
            sql.push(',');
        }
    }

    // Removes a trailing comma.
    if sql.ends_with(',') {
        sql.pop();
    }

    sql.push_str(" FROM ");
    sql.push_str(table);

    sql
}

/// Generates an SQL update statement based of LrAU permissions.
#[must_use]
pub fn lrau_sql_update(
    table: &str,
    user: &User,
    condition: &str,
    bind_offset: usize,
    correlations: &[LrAUSqlCorrelation],
) -> String {
    let mut sql = String::from("UPDATE ");
    sql.push_str(table);
    sql.push_str(" SET ");

    let mut n = bind_offset + 1;
    for cor in correlations {
        // Adds in all the column names.
        if lrau::User::get_valid_permissions(user, cor.perm, true)
            .unwrap_or(false)
        {
            sql.push_str(&format!("{} = ${},", cor.col, n));
            n += 1;
        }
    }

    // Removes a trailing comma.
    if sql.ends_with(',') {
        sql.pop();
    }

    sql.push(' ');
    sql.push_str(condition);

    sql
}

/// Generates an SQL insert into statement based of LrAU permissions.
#[must_use]
pub fn lrau_sql_insert(
    table: &str,
    user: &User,
    correlations: &[LrAUSqlCorrelation],
) -> String {
    let mut sql_column_names = format!("INSERT INTO {} (", table);
    let mut sql_values = String::from("VALUES (");

    let mut n = 1;
    for cor in correlations {
        // Adds in all the column names.
        if lrau::User::get_valid_permissions(user, cor.perm, true)
            .unwrap_or(false)
        {
            sql_column_names.push_str(cor.col);
            sql_values.push('$');
            sql_values.push_str(&n.to_string());
            sql_values.push(',');
            sql_column_names.push(',');
            n += 1;
        }
    }

    // Removes a trailing comma.
    if sql_column_names.ends_with(',') {
        sql_column_names.pop();
    }

    if sql_values.ends_with(',') {
        sql_values.pop();
    }

    // Add some closing brackets
    sql_column_names.push(')');
    sql_values.push(')');

    // Merge the two values
    let mut sql = sql_column_names;
    sql.push(' ');
    sql.push_str(&sql_values);

    sql
}
