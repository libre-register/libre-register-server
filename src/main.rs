#![warn(clippy::pedantic)]
#![allow(
    clippy::module_name_repetitions,
    clippy::wildcard_imports,
    clippy::used_underscore_binding,
    clippy::missing_errors_doc
)]

#[macro_use]
extern crate rocket;

use lrsvr::*;

#[launch]
async fn rocket_start() -> _ {
    // Allows dir specific environements.
    dotenv::dotenv().ok();

    // Start the logger.
    env_logger::init();

    // Gets the state
    let state = state::State::new().await;

    // Sets the port
    let config = rocket::Config {
        port: 20307,
        ..rocket::Config::default()
    };

    // Starts rocket
    rocket::build().manage(state).configure(config).mount(
        "/",
        routes![
            index_plaintext,
            index_json,
            icon,
            login::login,
            login::logout,
            login::get_permissions_by_session,
            login::get_permissions_by_username,
            login::create_user,
            login::reset_password,
            login::get_login_listing,
            login::reset_permissions,
            login::delete_user,
            login::assign_uid,
            login::get_uid,
            contacts::create_contact,
            contacts::get_contact,
            contacts::update_contact,
            contacts::delete_contact,
            contacts::listing::get_contact_listing,
        ],
    )
}
