#[derive(Debug)]
pub struct InternalServerError(pub String);
