use rocket::http::Header;

#[derive(Responder)]
#[response(status = 401)]
pub struct InvalidSession {
    /// There is no need for any information, all should be conveyed
    /// through headers and through status codes.
    inner: &'static str,
    /// The header, this should be set to:
    ///
    /// ```txt
    /// WWW-Authenticate: None
    /// ```
    ///
    /// Because Libre Register uses custom based authentication.
    header: Header<'static>,
}

impl Default for InvalidSession {
    /// Creates a new `InvalidSession` response with the correct
    /// types (see struct documentation.)
    fn default() -> Self {
        Self {
            inner: "INVALID_SESSION",
            header: Header::new("WWW-Authenticate", "None"),
        }
    }
}

impl InvalidSession {
    /// Creates a new invalid session for when a user is logged
    /// in but with insufficient permissions.
    ///
    /// If this is not desired than use
    /// [`new_not_logged_in`](Self::new_not_logged_in) instead.
    #[must_use]
    pub fn invalid_permissions() -> Self {
        Self {
            inner: "INVALID_PERMISSIONS",
            ..Self::default()
        }
    }

    /// Creates a new invalid session for when a user has an
    /// invalid session and so whatever they are attempting will
    /// not work.
    #[must_use]
    pub fn invalid_session() -> Self {
        Self::default()
    }
}
