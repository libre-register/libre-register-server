use rocket::response::Responder;

/// A Server error.
#[derive(Responder, Clone, Copy, Default, Hash)]
#[response(status = 500)]
pub struct ServerError(pub ());
