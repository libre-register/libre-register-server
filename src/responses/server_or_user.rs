use rocket::response::Responder;

/// A Server or user error.
///
/// This means that the error will either be a user error, or something
/// that goes wrong when fetching from the database.
#[derive(Responder)]
pub enum SOUError<T> {
    User(T),
    #[response(status = 500)]
    Server(()),
}

impl<T> From<T> for SOUError<T> {
    fn from(err: T) -> Self {
        Self::User(err)
    }
}

impl<T> SOUError<T> {
    #[must_use]
    pub fn new_server() -> Self {
        Self::Server(())
    }
}
