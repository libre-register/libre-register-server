pub use lr_commons::server_info::*;

lazy_static! {
    pub static ref LIBRE_REGISTER_SERVER: LibreRegisterServer =
        LibreRegisterServer {
            id: "LIBRE_REGISTER_SERVER".to_string(),
            version: "0.0.1-prealpha".to_string(),
        };
}
lazy_static! {
    pub static ref LIBRE_REGISTER_SERVER_STR: String =
        LIBRE_REGISTER_SERVER.to_string();
}
