#![warn(clippy::pedantic)]
#![allow(
    clippy::module_name_repetitions,
    clippy::wildcard_imports,
    clippy::used_underscore_binding,
    clippy::doc_markdown,
    clippy::missing_errors_doc,
    clippy::missing_panics_doc,
    clippy::too_many_lines
)]

#[macro_use]
extern crate serde;

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate lazy_static;

pub mod contacts;
pub mod error;
pub mod login;
#[macro_use]
pub mod macros;
pub mod responses;
pub mod server_info;
pub mod sql;
pub mod state;

#[get("/", format = "application/json", rank = 1)]
#[must_use]
pub fn index_json(
) -> rocket::serde::json::Json<&'static server_info::LibreRegisterServer> {
    rocket::serde::json::Json(&server_info::LIBRE_REGISTER_SERVER)
}

#[get("/", format = "text/plain")]
#[must_use]
pub fn index_plaintext() -> &'static str {
    &server_info::LIBRE_REGISTER_SERVER_STR
}

#[get("/favicon.ico", format = "image/webp")]
#[must_use]
pub fn icon() -> &'static [u8] {
    include_bytes!("../data/LibreRegister.webp")
}
